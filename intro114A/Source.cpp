#include<iostream>
#include<fstream>

using namespace std;

bool petriciumus(int k, int l, int* importance);

int main()
{	
	int k, l, importance=0;
	cin >> k >> l;
	if (petriciumus(k, l, &importance))
		cout << "YES" << endl << importance;
	else cout << "NO";	
}
bool petriciumus(int k, int l, int* importance)
{
	if ((k < l)&&(l%k==0))
	{
		(*importance)++;
		return petriciumus(k, l/k, importance);
	}
	else if (k == l)return true;
	else return false;
}